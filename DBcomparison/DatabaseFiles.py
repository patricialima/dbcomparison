'''
Created on 02/02/2014

@author: Ana
'''
from bioservices import kegg
import xml.etree.ElementTree as ET
from utils import readGenome,revcom
from Bio import Entrez
                
def ecocyc():
    Positions = open('ecocyc_file.txt','w')
    tree = ET.parse('ecocycgene.xml')
    root = tree.getroot()
    for item in root[1:]:
        try:
            try:
                genelocus = item.findtext("accession-1")
                genename = item.findtext("common-name")
                header = ">" + genelocus + " " + genename
                seq_from = item.findtext('left-end-position')
                seq_to = item.findtext('right-end-position')
                if item.findtext("transcription-direction") == "-":
                    final_seq = revcom(readGenome().seq[int(seq_from):int(seq_to)+1].upper())
                elif item.findtext("transcription-direction") == "+":
                    final_seq = readGenome().seq[int(seq_from):int(seq_to)+1].upper()
                to_write = str(header + "\n" + final_seq + "\n")
            except:
                genename = item.findtext("common-name")
                header = ">" + genename
                seq_from = item.findtext('left-end-position')
                seq_to = item.findtext('right-end-position')
                if item.findtext("transcription-direction") == "-":
                    final_seq = revcom(readGenome().seq[int(seq_from):int(seq_to)+1].upper())
                elif item.findtext("transcription-direction") == "+":
                    final_seq = readGenome().seq[int(seq_from):int(seq_to)+1].upper()
                to_write = str(header + "\n" + final_seq + "\n")
        except:
            try:
                centisome = item.findtext('centisome-position')
                header = ">" + centisome
                seq_from = item.findtext('left-end-position')
                seq_to = item.findtext('right-end-position')
                if item.findtext("transcription-direction") == "-":
                    final_seq = revcom(readGenome().seq[int(seq_from):int(seq_to)+1].upper())
                elif item.findtext("transcription-direction") == "+":
                    final_seq = readGenome().seq[int(seq_from):int(seq_to)+1].upper()
                to_write = str(header + "\n" + final_seq + "\n")
            except:
                to_write = str("")
        Positions.write(to_write)
    Positions.close()
      

def kegg():
    genes = []
    ecoli = kegg()
    myfile = open("T00007.txt", "r")
    lines = myfile.readlines()
    for line in lines:
        genes_id = line
        genes.append(genes_id[0:9])
    DB = open("genesKegg.txt","w")
    for gene in genes:
        a = ecoli.get(gene, "ntseq")
        DB.write(a + '\n')
    DB.close()
    
def keggDB():
    file1 = open("genesKegg.txt","r")
    file2 = open("kegg_file.txt","w")
    lines = file1.readlines()
    for line in lines:
        if line[0] != ">":
            sequence = line.replace("\n","").upper()
            file2.write(sequence)
        else:
            header = "\n" + ">" + line[5:15] + "\n"
            file2.write(header)
    file2.close()


def ncbi():
    Entrez.email = "anapatricialima3@gmail.com"
    Entrez.tool = "MyLocalScript"
    handle = open("ncbi_gene.xml", "r")
    ncbi_file = open("ncbi_file.txt","w")
    records = Entrez.parse(handle) 
    for record in records:
        try:
            genelocus = record["Entrezgene_gene"]["Gene-ref"]["Gene-ref_locus-tag"]
            genename = record["Entrezgene_gene"]["Gene-ref"]["Gene-ref_locus"]
            header = ">" + genelocus + " " + genename
            try:
                seq_from = record["Entrezgene_locus"][0]['Gene-commentary_seqs'][0]["Seq-loc_int"]["Seq-interval"]["Seq-interval_from"]
                seq_to = record["Entrezgene_locus"][0]['Gene-commentary_seqs'][0]["Seq-loc_int"]["Seq-interval"]["Seq-interval_to"]
                strand = record["Entrezgene_locus"][0]['Gene-commentary_seqs'][0]["Seq-loc_int"]["Seq-interval"]['Seq-interval_strand']['Na-strand'].attributes["value"]
                if strand == 'minus':
                    final_seq = revcom(readGenome().seq[int(seq_from):int(seq_to)+1])
                elif strand == 'plus':
                    final_seq = readGenome().seq[int(seq_from):int(seq_to)+1]
            except:
                seq_from = record["Entrezgene_locus"][0]['Gene-commentary_seqs'][0]['Seq-loc_mix']['Seq-loc-mix'][0]['Seq-loc_int']['Seq-interval']['Seq-interval_from']
                seq_to = record["Entrezgene_locus"][0]['Gene-commentary_seqs'][0]['Seq-loc_mix']['Seq-loc-mix'][0]['Seq-loc_int']['Seq-interval']['Seq-interval_to']
                strand = record["Entrezgene_locus"][0]['Gene-commentary_seqs'][0]["Seq-loc_int"]["Seq-interval"]['Seq-interval_strand']['Na-strand'].attributes["value"]
                if strand == 'minus':
                    final_seq = revcom(readGenome().seq[int(seq_from):int(seq_to)+1])
                elif strand == 'plus':
                    final_seq = readGenome().seq[int(seq_from):int(seq_to)+1]
            to_write = str(header + "\n" + final_seq + "\n")
        except:
            continue
        ncbi_file.write(to_write)
    ncbi_file.close() 


def regulon():
    reg_file = open("regulon_file.txt","w")
    tree = ET.parse('regulon.xml')
    root = tree.getroot()
    for item in root[0:]:
        if item.findtext("Type") == " Pseudo Gene" or item.findtext("Type") == " Phantom Gene" :
            to_write = str("") 
        try:
            if "(obsolete)" in list(item)[3].findtext("Synonyms"):
                to_write = str("") 
            try:
                genelocus =  list(item)[3].findtext("Synonyms") 
                genename = item.findtext("Name")
                if genelocus[0] != "b":
                    header = ">" + genename
                else:
                    header = ">" + genelocus + " " + genename
                    seq = item.findtext("Sequence")
                to_write = str(header + "\n" + seq + "\n")
            except:
                genename = item.findtext("Name")
                header = ">" + genename 
                seq = item.findtext("Sequence")
                to_write = str(header + "\n" + seq + "\n")     
        except:
            continue
        reg_file.write(to_write)
    reg_file.close()
      

def createDBfiles():
    ecocyc(), keggDB(), regulon(), ncbi()
    print "All files from the four biological databases are done"


