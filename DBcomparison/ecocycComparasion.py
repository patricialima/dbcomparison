'''
Created on 12/04/2014

@author: Ana
'''


from DBinformation import dicio_ecocyc,dicio_kegg, dicio_ncbi,dicio_regulon
from utils import revcom

def compareEco():
    myfile = open("ComparacaoDaEcocyc.csv","w")
    eco = dicio_ecocyc()
    ncbi = dicio_ncbi()
    kegg = dicio_kegg()
    regulon = dicio_regulon()
    header = "Ecocyc,,NCBI,,,Ecocyc,,Regulon,,,Ecocyc,,KEGG,,,Matches\n"
    myfile.write(header)
    for key in eco.keys():
        a = [key, '-', ' ' , ' ', ' ', ' ', ' ', 0]
        for nkey in ncbi.keys():
            if key in nkey:
                a[1] = nkey
                a[4] = 'KEY'
                if eco[key][300:-300] in ncbi[nkey] or eco[key][300:-300] in revcom(ncbi[nkey]) :
                    a[4] = 'Match'
                    a[7] +=1
                    break
                break
        for rkey in regulon.keys():
            if rkey in key:
                a[2] = nkey
                a[5] = 'KEY'
                if eco[key][300:-300] in regulon[rkey] or  eco[key][300:-300] in revcom(regulon[rkey]):
                    a[5] = 'MATCH'
                    a[7] += 1
                    break                    
                break
        for kkey in kegg.keys():
            if key in kkey:
                a[3] = kkey
                a[6] = 'KEY'
                if eco[key][300:-300] in kegg[kkey]:
                    a[6] = 'MATCH'
                    a[7] += 1
                    break
                break
        line = '%s,%s,%s,,,%s,%s,%s,,,%s,%s,%s,,,%s\n' % (a[0],a[4],a[1],a[0],a[5],a[2],a[0],a[6],a[3],str(a[7]))
        myfile.write(line)
    myfile.close()


