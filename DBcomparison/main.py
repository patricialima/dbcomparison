'''
Created on 24/04/2015

@author: Ana
'''
from DatabaseFiles import createDBfiles
from DBinformation import create_dicionarios
from comparisonDb import compare


if __name__ == '__main__':
    createDBfiles()
    create_dicionarios()
    compare()
    print 'ficheiro de comparacao is already done'