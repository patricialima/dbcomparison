'''
Created on 02/04/2014

@author: Ana
'''


def dicio_regulon():
    regulon = open("regulon_file.txt","r")
    lines = regulon.readlines()
    regulondict = {}
    key = ""
    sequence = ""
    for line in lines:
        if line[0] == ">":
            sequence = ""
            key = line[1:].replace("\t","").replace("\n","").replace(";","")
        if line[0] != ">":
            sequence += line 
            sequence = sequence.replace("\t","").replace("\n","").replace(";","")
            regulondict[key] = sequence
    return regulondict


def dicio_kegg():
    kegg = open("kegg_file.txt","r")
    lines = kegg.readlines()
    keggdict = {}
    key = ""
    sequence = ""
    for line in lines:
        if line[0] == ">":
            sequence = ""
            key = line[1:].replace("\t","").replace("\n","").replace(";","")
        if line[0] != ">":
            sequence += line 
            sequence = sequence.replace("\t","").replace("\n","").replace(";","")
            keggdict[key] = sequence
    return keggdict


 
def dicio_ecocyc():
    ecocyc = open("ecocyc_file.txt","r")
    lines = ecocyc.readlines()
    ecocycdict = {}
    key = ""
    sequence = ""
    for line in lines:
        if line[0] == ">":
            sequence = ""
            key = line[1:].replace("\t","").replace("\n","").replace(";","")     
        if line[0] != ">":
            sequence += line 
            sequence = sequence.replace("\t","").replace("\n","").replace(";","")
            ecocycdict[key] = sequence.upper()
    return ecocycdict


def dicio_ncbi():
    ncbi = open("ncbi_file.txt","r")
    lines = ncbi.readlines()
    ncbidict = {}
    key = ""
    sequence = ""
    for line in lines:
        if line[0] == ">":
            sequence = ""
            key = line[1:].replace("\t","").replace("\n","").replace(";","")
        if line[0] != ">":
            sequence += line 
            sequence = sequence.replace("\t","").replace("\n","").replace(";","")
            ncbidict[key] = sequence
    return ncbidict




def create_dicionarios():
    dicio_ecocyc(), dicio_kegg(), dicio_ncbi(), dicio_regulon()
    print "diciona'rios: check!"
