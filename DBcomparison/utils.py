'''
Created on 24/04/2015

@author: Ana
'''
from Bio import SeqIO, Seq

def readGenome():
    return SeqIO.read("EcoliK12MG1655genome.fasta", format="fasta")

def complement(s): 
    basecomplement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'} 
    letters = list(s) 
    letters = [basecomplement[base] for base in letters] 
    return ''.join(letters)

def revcom(s):
    return complement(s[::-1].upper())


def counting():
    n = open("ecocyc_file.txt","r")
    lines = n.readlines()
    count = 0
    for line in lines:
        if line[0:3] == ">b0":
            count +=1
        elif line[0:3] == ">b1":
            count +=1
        elif line[0:3] == ">b2":
            count +=1
        elif line[0:3] == ">b3":
            count +=1
        elif line[0:3] == ">b4":
            count +=1
        else:
            if line[0] == ">":
                print line 
    print count
    