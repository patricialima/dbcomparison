'''
Created on 24/02/2014

@author: Ana
'''

from DBinformation import dicio_ecocyc,dicio_kegg,dicio_ncbi,dicio_regulon
from utils import revcom

def compare():
    eco = dicio_ecocyc()
    ncbi = dicio_ncbi()
    kegg = dicio_kegg()
    regulon = dicio_regulon()
    myfile = open("ComDB.csv","w")
    header = "Ecocyc,,NCBI,,,Ecocyc,,Regulon,,,Ecocyc,,KEGG,,,NCBI,,Regulon,,,NCBI,,KEGG,,,Regulon,,KEGG,,,Match\n"
    myfile.write(header)
    for keys in eco.keys():
        a = [keys,'','','','','','','','','',0]
        ker =  False #chave do regulon
        knc = False #chave do ncbi
        kke = False #chave kegg
        snc = False #sequencia do ncbi
        ske = False #sequencia do kegg
        ser = False #sequencia do regulon
        '''Comparacao de Ecocyc com o NCBI'''
        for nkey in ncbi.keys():
            if nkey in keys:
                knc = True
                a[1] = nkey
                a[4] = 'Gene'
                if eco[keys][500:-500] in ncbi[nkey] or revcom(eco[keys])[500:-500]in ncbi[nkey]:
                    snc = True # a sequencia do Gene no ecocycComparasion passa a true
                    a[4] = 'Match'
                    a[10] += 1 #numero de Match
                    break
                break 
        '''Comparacao de Ecocyc com o Regulondb'''    
        for rkey in regulon.keys():
            if rkey in keys:
                ker = True
                a[2] = rkey
                a[5] = 'Gene'
                if eco[keys][500:-500] in regulon[rkey] or revcom(eco[keys])[500:-500] in regulon[rkey]:
                    ser = True
                    a[5] = 'Match'
                    a[10] +=1
                    break
                break
        '''Comparacao do Ecocyc com o KEGG'''  
        for k in kegg.keys():
            if k in keys:
                kke = True
                a[3] = k
                a[6] = 'Gene'
                if kegg[k][500:-500] in eco[keys] or revcom(kegg[k])[500:-500] in eco[keys]:
                    ske = True
                    a[6] = 'Match'
                    a[10] += 1
                    break
                break
        ''' Comparacao do NCBI com o Regulon'''
        if ker and knc:
            a[7]='Gene'
            if regulon[rkey][500:-500] in ncbi[nkey] or revcom(regulon[rkey])[500:-500] in ncbi[nkey]:
                a[7] = 'Match'
                a[10] +=1
        '''Comparacao do NCBI com o KEGG'''
        if kke and knc:
            a[8]='Gene'
            if kegg[k]in ncbi[nkey] or revcom(kegg[k]) in ncbi[nkey]:
                a[8] = 'Match'
                a[10] +=1     
        '''Comparacao do KEGG com o Regulon'''
        if kke and ker:
            a[9] = 'Gene'
            if regulon[rkey][500:-500] in kegg[k] or revcom(regulon[rkey])[500:-500] in kegg[k]:
                a[9] = 'Match'
                a[10] += 1
        line = "%s,%s,%s,,,%s,%s,%s,,,%s,%s,%s,,,%s,%s,%s,,,%s,%s,%s,,,%s,%s,%s,,,%s\n" %(a[0],a[4],a[1],a[0],a[5],a[2],a[0],a[6],a[3],a[1],a[7],a[2],a[1],a[8],a[3],a[2],a[9],a[3],str(a[10]))
        myfile.write(line)
    myfile.close()

